using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase.Database;
using UnityEngine;
using UnityEngine.UI;

public class Search : MonoBehaviour
{
    public event Action<List<Note>> Searching;
    public string SearchText => search.text;
    private string tempText;
    [SerializeField] private InputField search;
    [SerializeField] private GameObject searchPanel;

    private void Awake()
    {
        tempText = search.text;
    }

    public void CloseOpen() 
    {
        search.text = default;
        searchPanel.SetActive(!searchPanel.activeSelf);
    }
    
    public void OnChanged()
    { 
        if (searchPanel.activeSelf)
        {
            tempText = search.text;
            Filter();
        }
    }

    public void OnEndEdit()
    {
        if (!searchPanel.activeSelf)
        {
            search.text = tempText;
            Filter();
        }
    }

    private async void Filter()
    {
        var uuid = Controller.Instance.User.Uuid;
        Task<DataSnapshot> searchTask;

        if (search.text == default) searchTask = Database.Get(uuid + "/notes");
        else
            searchTask = Database.SearchNote(uuid, search.text);
        await searchTask;
        var notes = new List<Note>();
        foreach (var item in searchTask.Result.Children)
        {
            var note = Database.Deserialize<Note>(item);
            notes.Add(note);
        }
        notes.Reverse();
        Searching?.Invoke(notes);
    }
}
