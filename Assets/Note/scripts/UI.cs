using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    [SerializeField] private Transform scrollViewContent;
    [SerializeField] private GameObject notePrefab;
    [SerializeField] private NewNote newNote;
    [SerializeField] private NewNote editNewNote;
    [SerializeField] private Search search;
    [SerializeField] private Sorting sorting;


    private void Start()
    {  
        sorting.SortList += InitSortNotes;
        search.Searching += InitSortNotes;
        Controller.Instance.Update += UpdateList;
    }

    //create a new note by click
    public void CreateNewNote()
    {
        newNote.Open();
    }
    //revision check of the note or is it new
    public void CloseNote()
    {
        if (newNote.NewNotePanel.activeSelf && newNote.IsOpen) CloseNoteView(true);
        if (newNote.EditNotePanel.activeSelf && editNewNote.IsOpen) CloseNoteView(false);
    }
    //close a new note by click
    private void CloseNewNoteView()
    {
        newNote.Close();
    }
    //if a new note is closed and saved
    //if the editorial note is closed and rewritten
    private void CloseNoteView(bool isNew)
    {
        if (isNew) newNote.Close();
        else
            editNewNote.CloseNote();
        PlayerPrefs.SetString("uuid", Controller.Instance.User.Uuid);
        PlayerPrefs.Save();
    }

    //clearing previous prefabs with notes
    private void ClearListNotes()
    {
        foreach (Transform child in scrollViewContent)
        {
            Destroy(child.gameObject);
        }
    }

    //creation of the note prefab
    private void InitNotes()
    {
        Controller.Instance.User.Notes.Reverse();

        foreach (var note in Controller.Instance.User.Notes)
        {
            var no = Instantiate(notePrefab, scrollViewContent).GetComponent<NoteObject>();
            no.Init(note, editNewNote);
        }
    }

    //rewriting prefabs to use search and sort
    private void InitSortNotes(List<Note> notes)
    {
        ClearListNotes();
        if (notes.Count == 0)
            return;
        foreach (var note in notes)
        {
            var no = Instantiate(notePrefab, scrollViewContent).GetComponent<NoteObject>();
            no.Init(note, editNewNote);
        }
    }

    private void UpdateList()
    {
        ClearListNotes();
        InitNotes();
    }
}
