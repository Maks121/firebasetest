using System;
using System.Text;
using System.Threading.Tasks;
using Firebase;
using Firebase.Database;
using Firebase.Unity;
using UnityEngine;

public static class Database
{
    private static readonly DatabaseReference reference;

    static Database()
    {
        FirebaseApp app = FirebaseApp.Create();
        reference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    public static Task Save(string json, string path)
    {
        return reference.Child(path).SetRawJsonValueAsync(json);
    }

    public static Task<DataSnapshot> Get(string path)
    {
       
        return reference.Child(path).GetValueAsync();
    }
    
    public static Task<DataSnapshot> SearchNote(string path, string searchText)
    {
        
        return reference.Child(path + "/notes/").OrderByChild("title").StartAt(searchText).EndAt(searchText + "\uf8ff").GetValueAsync();
    }

    public static Task Remove(string path)
    {
        return reference.Child(path).RemoveValueAsync();
    }

    public static T Deserialize<T>(DataSnapshot snapshot) => JsonUtility.FromJson<T>(snapshot.GetRawJsonValue());
}
