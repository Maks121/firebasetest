using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using Firebase.Auth;
using static Firebase.Auth.FirebaseAuth;



public static class Authentication
{
    private static readonly FirebaseAuth auth;
    
    static Authentication()
    {
        auth = FirebaseAuth.DefaultInstance;
    }
    
    public static Task<FirebaseUser> SignUp(string email,string password)
    {
        return auth.CreateUserWithEmailAndPasswordAsync(email, password);
    }
    
    public static Task<FirebaseUser> SignIn(string email, string password)
    {
        return auth.SignInWithEmailAndPasswordAsync(email, password);
    }

}
