using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    public event Action Update;
    public static Controller Instance;
    [SerializeField] private User user;
    public User User => user;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        user = null;
    }
    //create new user in Firebase with using email(..@gmail.com), password(******)
    public async Task<string> SignUp(string email, string password)
    {
        var userTask = Authentication.SignUp(email, password);
        await userTask;
        var uuid = userTask.Result.UserId;
        user = new User(uuid);
        UpdateDatabase();
        return uuid;
    }

    //singin with creating user in Firebase
    public async Task<string> SignIn(string email, string password)
    {
        var authTask = Authentication.SignIn(email, password);
        await authTask;
        var uuid = authTask.Result.UserId;
        var userTask = Database.Get(uuid);
        await userTask;
        user = Database.Deserialize<User>(userTask.Result);
        Update?.Invoke();
        return uuid;
    }

    public void EditNote(Note note)
    {
        user.EditNote(note);
        UpdateDatabase();
    }

    public void AddNote(Note note)
    {
        user.AddNote(note);
        UpdateDatabase();
    }

    public void DeleteNote(Note note)
    {
        user.DeleteNote(note);
        UpdateDatabase();
    }

    public void UpdateDatabase()
    {
        Database.Save(user.ToString(), user.Uuid);
        Update?.Invoke();
    }
}
