using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Sorting : MonoBehaviour
{
    public event Action<List<Note>> SortList;
    [SerializeField] private GameObject sortPanel;
    [SerializeField] private Toggle dataName;
    [SerializeField] private Toggle ascendingDescending;
    private bool date;

    public void CloseOpenSort()
    {
        sortPanel.SetActive(!sortPanel.activeSelf);
    }
    //take changes in toggle 
    public void OnChanged() 
    {
        sortDateName();
    }

    //sort by date and name, descending and ascending
    private void sortDateName()
    {
        var notes = Controller.Instance.User.Notes;
        IOrderedEnumerable<Note> sortedNotes = null;
        if (dataName.isOn) date = true;
            else
                 date = false;

        if (ascendingDescending.isOn)
        {
            if (date)
            {
                sortedNotes = notes.OrderBy(n => n.Date);
            }
            else
            {
                sortedNotes = notes.OrderBy(n => n.Title);
            }
        }
            else
            {
                if (date)
                {
                    sortedNotes = notes.OrderByDescending(n => n.Date);
                }
                    else
                    {
                         sortedNotes = notes.OrderByDescending(n => n.Title);
                    }
            }
        SortList?.Invoke(sortedNotes.ToList());
    }
}
