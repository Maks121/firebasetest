using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewNote : MonoBehaviour
{
    public string Title => title.text;
    public string Description => description.text;
    public GameObject NewNotePanel;
    public GameObject EditNotePanel;
    [SerializeField] private InputField title;
    [SerializeField] private InputField description;
    [SerializeField] private ImageController imageController;
    [SerializeField] private Note currentNote;
    public bool IsOpen = false;
    private string noteTitle;
    private string noteDescription;

    public void Open()
    {
        NewNotePanel.SetActive(!NewNotePanel.activeSelf);
        IsOpen = true;
        currentNote = new Note();
        imageController.InitNote(currentNote);
    }

    //reading text
    public void OnChanged()
    {
        if (NewNotePanel.activeSelf) 
        { 
        noteTitle = Title;
        noteDescription = Description;
        }
    }

    //opening a note and comparing it with the current one
    public void OpenNote(Note note)
    {
        Open();
        currentNote = note;
        imageController.InitNote(currentNote);
        title.text = note.Title;
        description.text = note.Description;
        IsOpen = true;
     }

    //close after editing
    public void CloseNote()
    {
        IsOpen = false;
        EditNotePanel.SetActive(!EditNotePanel.activeSelf);

        if (currentNote.Title != noteTitle || currentNote.Description != noteDescription)
        {
            SaveNote();
        }
        Controller.Instance.EditNote(currentNote);
    }

    public void DeleteNote()
    {
        IsOpen = false;
        EditNotePanel.SetActive(!NewNotePanel.activeSelf);
        Controller.Instance.DeleteNote(currentNote);
    }

    //close and save a new note
    public void Close()
    {
        IsOpen = false;
        SaveNote();
        NewNotePanel.SetActive(!NewNotePanel.activeSelf);
        Controller.Instance.AddNote(currentNote);
        title.text = default;
        description.text = default;
    }

    private void SaveNote()
    {
        currentNote.Title = noteTitle;
        currentNote.Description = noteDescription;
        currentNote.Date = DateTime.Now.ToString();
    }
}
