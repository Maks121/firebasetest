using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class User
{
    [SerializeField] private string uuid;
    [SerializeField] private List<Note> notes;
    [SerializeField] public List<Note> Notes => notes;
    public string Uuid => uuid;

    public User()
    {
        uuid = Guid.NewGuid().ToString();
        notes = new List<Note>();
    }

    public User(string uuid)
    {
        this.uuid = uuid;
        notes = new List<Note>();
    }

    public void AddNote(Note note)
    {
        notes.Add(note);
    }

    public void EditNote(Note note)
    {
        var index = notes.IndexOf(note);
        notes[index] = note;
    }

    public void DeleteNote(Note note)
    {
        notes.Remove(note);
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
