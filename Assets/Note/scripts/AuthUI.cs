using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class AuthUI : MonoBehaviour
{
    [SerializeField] private GameObject LOGPanel;
    [SerializeField] private GameObject Back;

    [SerializeField] private InputField emailInput;
    [SerializeField] private InputField PasswordInput;

    private string email => emailInput.text;
    private string password => PasswordInput.text;

    [SerializeField] private Text emailText;
    [SerializeField] private Text InEmail;

    public void Awake()
    {
        if (PlayerPrefs.HasKey("email")) emailText.text = PlayerPrefs.GetString("email");
    }

    public void OpenNotes()
    {
        LOGPanel.SetActive(!LOGPanel.activeSelf);
        Back.SetActive(!Back.activeSelf);
    }

    //create new user by click
    public async void SignUpButton()
    {
        InputIsEmpty();

        var authTask = Controller.Instance.SignUp(email, password);
        await authTask;
        var uuid = authTask.Result;
        PlayerPrefs.SetString("uuid", uuid);
        PlayerPrefs.SetString("email", email);
        emailText.text = email;
        InEmail.text = "Create new user " + email;
        PlayerPrefs.Save();
       
    }

    //Sing in by click
    public async void SignInButton()
    {
        InputIsEmpty();
        var uuidTask = Controller.Instance.SignIn(email, password);
        await uuidTask;
        var uuid = uuidTask.Result;
        PlayerPrefs.SetString("uuid", uuid);
        PlayerPrefs.SetString("email", email);
        emailText.text = email;
        PlayerPrefs.Save();
        OpenNotes();
    }

    //exit by click
    public void LogOut()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        LOGPanel.SetActive(!LOGPanel.activeSelf);
        Back.SetActive(!Back.activeSelf);

    }

    //checking if the lines are full
    private void InputIsEmpty()
    {
        if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
        {
            return;
        }
    }

}
