using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Note
{
    [SerializeField] private string uuid;
    [SerializeField] private string title;
    [SerializeField] private string description;
    [SerializeField] private string date;

    public string Uuid => uuid;
    public string Title { get => title; set => title = value; }
    public string Description { get => description; set => description = value; }
    public string Date { get => date; set => date = value; }

    public Note()
    {
        uuid = Guid.NewGuid().ToString();
        date = DateTime.Now.ToString();
    }

    public Note(string uuid, string title, string description, string date)
    {
        this.uuid = uuid;
        this.title = title;
        this.description = description;
        this.date = date;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
