using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class NoteObject : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private TextMeshProUGUI date;
    [SerializeField] private Note note;
    [SerializeField] private NewNote newNote;
    public string Title => title.text;
    public string Description => description.text;

    //displaying note text on a perfub
    public void Init(Note note, NewNote newNote)
    {
        this.note = note;
        title.text = note.Title;
        description.text = note.Description;
        date.text = note.Date;
        this.newNote = newNote;
    }
    //opening a note by clicking on the prefab
    public void Open()
    {
        newNote.OpenNote(note);
    }

}
