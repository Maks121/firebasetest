﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using NativeGalleryNamespace;
using static NativeGallery;

public class ImageController : MonoBehaviour
{
	[SerializeField] private Storage upload;
	[SerializeField] private Note curruntNote;
	[SerializeField] private Texture2D texture;

	//get the current note Uuid
	public void InitNote(Note note)
    {
		curruntNote = note;
    }

	//opening the gallery by clicking the button and uploading to the database
	public void PickImage()
	{
		Permission permission = GetImageFromGallery((path) =>
		{
			Debug.Log("Image path: " + path);
			if (path != null)
			{
				Texture2D texture = new Texture2D(2, 2, TextureFormat.Alpha8, false);
			    texture.LoadImage(File.ReadAllBytes(path));
				upload.StartUpload(texture,curruntNote);
			}

		}, "Select a PNG image", "image/png");
	}
}