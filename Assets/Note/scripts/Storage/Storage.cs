﻿using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Storage;
using UnityEngine;

public class Storage : MonoBehaviour
{
    public void StartUpload(Texture2D texture, Note note)
    {
        StartCoroutine(Upload(texture, note));
    }
    //uploading an image to firebase
    private IEnumerator Upload(Texture2D texture, Note note)
    {
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;
        StorageReference imageReference = storage.GetReference($"image/{Controller.Instance.User.Uuid}/{note.Uuid}/image.jpg");
        var newMetadata = new MetadataChange();
        newMetadata.ContentType = "image/jpeg";
        var bytes = texture.EncodeToJPG();
        imageReference.PutBytesAsync(bytes, newMetadata, null);
        //imageReference.UpdateMetadataAsync(newMetadata);
        var uploadTask = imageReference.PutBytesAsync(bytes);
        yield return new WaitUntil(() => uploadTask.IsCompleted);

        if (uploadTask.Exception != null)
        {
            Debug.LogError($"Failed to upload:{uploadTask.Exception}");
            yield break;
        }
    }
}
